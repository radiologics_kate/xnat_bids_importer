package com.radiologics.bids.dataimport;

import com.radiologics.bids.observer.ImportObserver;
import com.radiologics.bids.utils.BIDSConstants;
import com.radiologics.bids.utils.BIDSFileUtils;
import org.nrg.xdat.bean.XnatDemographicdataBean;
import org.nrg.xdat.bean.XnatSubjectdataBean;
import org.nrg.xdat.bean.XnatSubjectdataFieldBean;
import org.nrg.xdat.bean.base.BaseElement;

import java.util.Enumeration;
import java.util.Properties;

public class SubjectParams extends XnatBeanParams {

    private Properties _properties;
    private String _subjectLabel;
    private XnatSubjectdataBean _subject;

    public SubjectParams(ImportObserver observer, Properties properties) {
        super(observer);
        _properties = properties;
        String participant_id = _properties.getProperty(BIDSConstants.PARTICIPANT_ID);
        _subjectLabel = BIDSFileUtils.clearPrefix(participant_id, BIDSConstants.SUBJECT_PREFIX);
    }
    public SubjectParams(ImportObserver observer, String subjectLabel) {
        super(observer);
        _properties = null;
        _subjectLabel = subjectLabel;
    }

    public XnatSubjectdataBean toSubjectBean(String projectId) {
        _subject = new XnatSubjectdataBean();
        _subject.setProject(projectId);
        _subject.setLabel(_subjectLabel);
        if (_properties == null) {
            return _subject;
        }
        try {
            // Slightly more complex case where parameters may be demographics or may be sub
            XnatDemographicdataBean demogData = new XnatDemographicdataBean();
            boolean hasDemog = false;
            Enumeration<?> keys = _properties.keys();
            String xnatParamName;
            BaseElement element = _subject;
            while (keys.hasMoreElements()) {
                xnatParamName = null;
                String key = (String) keys.nextElement();
                String field = key.toLowerCase();
                String value = _properties.getProperty(key);
                if (BIDSConstants.SUBJECT_DEMOG_FIELDS_MAP.containsKey(field)) {
                    hasDemog = true;
                    xnatParamName = BIDSConstants.SUBJECT_DEMOG_FIELDS_MAP.get(field);
                    element = demogData;
                } else if (BIDSConstants.SUBJECT_FIELDS_MAP.containsKey(field)) {
                    xnatParamName = BIDSConstants.SUBJECT_FIELDS_MAP.get(field);
                    element = _subject;
                }
                doAddParam(key, value, xnatParamName, element, BIDSConstants.SUBJECT_DEMOG_TRANSFORM_MAP);
            }
            if (hasDemog) {
                _subject.setDemographics(demogData);
            }
        } catch (Exception e) {
            observer.error("Issue parsing subject properties for " +
                    _subjectLabel + ": " + e.getMessage(), e);
        }
        return _subject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void addParamCmd(String key, String value) { }

    /**
     * {@inheritDoc}
     */
    @Override
    void addParamAsAdditional(String key, String value) throws Exception {
        XnatSubjectdataFieldBean field = new XnatSubjectdataFieldBean();
        field.setName(key);
        field.setField(value);
        _subject.addFields_field(field);
    }

}
