package com.radiologics.bids.dataimport;

import com.radiologics.bids.observer.ImportObserver;
import org.nrg.xdat.bean.base.BaseElement;

import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.function.BiFunction;

public abstract class XnatBeanParams {
    ImportObserver observer;

    public XnatBeanParams(ImportObserver observer) {
        this.observer = observer;
    }

    /**
     * Use transform map's function to change/clean/etc input before add
     * @param xnatParamName xnat's function name for setting the parameter
     * @param value the parameter value
     * @param element the element to which the param is to be added
     * @param map the transform map
     * @return the cleaned param
     */
    String cleanValueForXnat(String xnatParamName, String value, BaseElement element,
                             Map<String, BiFunction<BaseElement, String, String>> map) {
        if (map.containsKey(xnatParamName)) {
            value = map.get(xnatParamName).apply(element, value);
        }
        return value;
    }

    /**
     * Add parameter as custom field
     * @param key the name
     * @param value the value
     * @throws Exception if issues
     */
    abstract void addParamAsAdditional(String key, String value) throws Exception;

    /**
     * Add param to XNAT element. Try to use XNAT setter (xnatSetterName), fall back on adding as additional parameter.
     * Call this method directly with the xnatSetterName defined if you have a more complicated mapping of downcased
     * BIDS field to XNAT setter names, otherwise, use
     * {@link #addParam(String, String, BaseElement, Map, Map) addParam} method.
     *
     * @param key the parameter name
     * @param value the parameter value
     * @param xnatSetterName the XNAT setter name
     * @param element the element to which the param is to be added
     * @param transformMap the mapping of XNAT setter name to transform function
     * @throws Exception for issues
     */
    void doAddParam(String key, String value, String xnatSetterName, BaseElement element,
                        Map<String, BiFunction<BaseElement, String, String>> transformMap) throws Exception {
        if (xnatSetterName != null) {
            String methodName = "set" + xnatSetterName;
            try {
                Method setNameMethod = element.getClass().getMethod(methodName, String.class);
                String v = cleanValueForXnat(xnatSetterName, value, element, transformMap);
                setNameMethod.invoke(element, v); // pass arg
            } catch (Exception e) {
                observer.error("Could not set the XNAT param " + methodName +
                        " value: " + value + " for  " + element.getSchemaElementName());
                addParamAsAdditional(key, value);
            }
        } else {
            addParamAsAdditional(key, value);
        }
    }

    /**
     * Add param to XNAT element. Try to use XNAT setter determined from fieldMap, fall back on adding as additional parameter.
     * @param key the parameter name
     * @param value the parameter value
     * @param element the element to which the param is to be added
     * @param fieldMap the mapping of downcased BIDS field to XNAT setter name
     * @param transformMap the mapping of XNAT setter name to transform function
     * @throws Exception upon failure
     */
    void addParam(String key, String value, BaseElement element,  Map<String, String> fieldMap,
                      Map<String, BiFunction<BaseElement, String, String>> transformMap) throws Exception {
        String field = key.toLowerCase();
        String xnatSetterName = fieldMap.getOrDefault(field, null);
        doAddParam(key, value, xnatSetterName, element, transformMap);
    }

    /**
     * Add parameters specified in properties
     * @param properties the properties
     */
    void addParamsFromProperties(Properties properties) {
        Enumeration<?> keys = properties.keys();
        while (keys.hasMoreElements()) {
            String fieldName = (String) keys.nextElement();
            try {
                addParamCmd(fieldName, properties.getProperty(fieldName));
            } catch (Exception e) {
                observer.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Caller for {@link #addParam(String, String, BaseElement, Map, Map) addParam} method.
     * @param key the parameter name
     * @param value the parameter value
     * @throws Exception if issues
     */
    abstract void addParamCmd(String key, String value) throws Exception;
}
