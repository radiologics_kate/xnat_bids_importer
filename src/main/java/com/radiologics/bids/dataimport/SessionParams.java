package com.radiologics.bids.dataimport;

import com.radiologics.bids.observer.ImportObserver;
import com.radiologics.bids.utils.BIDSConstants;
import com.radiologics.bids.utils.BIDSFileUtils;
import org.nrg.xdat.bean.XnatExperimentdataFieldBean;
import org.nrg.xdat.bean.XnatMrsessiondataBean;

import java.util.Properties;

public class SessionParams extends XnatBeanParams{
    private Properties _properties;
    private String _visitId;
    private XnatMrsessiondataBean _session;

    public SessionParams(ImportObserver observer, Properties properties) {
        super(observer);
        _properties = properties;
        String sessionId = _properties.getProperty(BIDSConstants.SESSION_ID);
        _visitId = BIDSFileUtils.clearPrefix(sessionId, BIDSConstants.SESSION_PREFIX);
    }
    public SessionParams(ImportObserver observer, String visitId) {
        super(observer);
        _properties = null;
        _visitId = visitId;
    }

    public XnatMrsessiondataBean toXnatMrSessionDataBean(String projectId, String subjectId, String subjectLabel) {
        _session = new XnatMrsessiondataBean();
        _session.setProject(projectId);
        _session.setSubjectId(subjectId);
        if (!_visitId.equals(subjectLabel)) {
            _session.setLabel(subjectLabel + "_" + _visitId);
            _session.setVisitId(_visitId);
        } else {
            _session.setLabel(_visitId);
        }
        if (_properties == null) {
            return _session;
        }
        try {
            addParamsFromProperties(_properties);
        } catch (Exception e) {
            observer.error("Issue parsing session properties for " +
                    _session.getLabel() + ": " + e.getMessage(), e);
        }
        return _session;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void addParamCmd(String key, String value) throws Exception {
        addParam(key, value, _session, BIDSConstants.MRSESSION_FIELDS_MAP, BIDSConstants.MRSESSION_TRANSFORM_MAP);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    void addParamAsAdditional(String key, String value) {
        XnatExperimentdataFieldBean field = new XnatExperimentdataFieldBean();
        field.setName(key);
        field.setField(value);
        _session.addFields_field(field);
    }
}
