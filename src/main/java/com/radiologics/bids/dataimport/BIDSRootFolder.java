package com.radiologics.bids.dataimport;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3URI;
import com.radiologics.bids.connection.RemoteConnection;
import com.radiologics.bids.connection.RemoteConnectionManager;
import com.radiologics.bids.manifest.ImportedItem;
import com.radiologics.bids.observer.ImportObserver;
import com.radiologics.bids.utils.BIDSConstants;
import com.radiologics.bids.utils.BIDSFileUtils;
import com.radiologics.filesystems.aws.s3.model.auto.AwsS3Config;
import com.radiologics.filesystems.aws.s3.services.AwsS3FilesystemService;
import com.radiologics.filesystems.exceptions.*;
import com.radiologics.filesystems.services.FilesystemService;
import com.radiologics.filesystems.services.LocalFilesystemService;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xdat.bean.XnatProjectdataBean;
import org.nrg.xft.utils.FileUtils;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author Mohana Ramaratnam
 */
public class BIDSRootFolder {

    private final RemoteConnectionManager _manager;
    private final RemoteConnection _connection;
    private final FilesystemService _filesystemService;
    private final String _rootFolderPath;
    @Nullable private final String _projectId;
    private final String _logPath;
    private final BIDSFileUtils _utils;
    private final boolean _externalFilesystem;
    private AtomicInteger rtnCode = new AtomicInteger(0);
    private final List<String> _allFiles;

    public BIDSRootFolder(final RemoteConnectionManager manager, RemoteConnection connection, String logPath,
                          String rootFolderPath, @Nullable String projectId, @Nullable String awsAccessKey,
                          @Nullable String awsSecretKey)
            throws IOException, FilesystemServiceException, InvalidEntityException {
        _manager = manager;
        _connection = connection;
        _rootFolderPath = rootFolderPath;
        _projectId = projectId;
        _logPath = logPath;

        setUpLogging();

        // Set up FilesystemService
        if (_rootFolderPath.startsWith(AmazonS3.ENDPOINT_PREFIX)) {
            _filesystemService = setUpAwsS3FilesystemService(awsAccessKey, awsSecretKey);
            _externalFilesystem = true;
        } else if (FileUtils.IsUrl(_rootFolderPath, true)) {
            throw new NoSuchFilesystemException("Non-S3 URLs not (yet) supported");
        } else {
            _filesystemService = setUpLocalFilesystemService();
            _externalFilesystem = false;
        }
        _utils = new BIDSFileUtils(_filesystemService.separator);
        _allFiles = _filesystemService.listAllFiles(_rootFolderPath, _projectId);
    }

    private void setUpLogging() throws IOException {
        // Set up logging
        File rootPath = new File(_logPath);
        if (!rootPath.exists()) {
            rootPath.mkdirs();
        }
        if (!rootPath.exists() || !rootPath.isDirectory())
            throw new IOException("Expecting a directory at this path " + _logPath);
    }

    private FilesystemService setUpLocalFilesystemService() throws IOException {
        //Does the root folder exist?
        File bidsRootDir = new File(_rootFolderPath);
        if (!bidsRootDir.exists() || !bidsRootDir.isDirectory()) {
            throw new IOException("Incorrect path: " + _rootFolderPath + " does not exist or is not a directory");
        }
        return new LocalFilesystemService();
    }

    private FilesystemService setUpAwsS3FilesystemService(String awsAccessKey, String awsSecretKey)
            throws RemoteApiException, InvalidEntityException {
        AmazonS3URI uri = new AmazonS3URI(_rootFolderPath);
        AwsS3Config config = new AwsS3Config(uri.getBucket(), awsAccessKey, awsSecretKey);
        config.initializeS3();
        return new AwsS3FilesystemService(config);
    }

    private String makeProjectId(String projectDir) {
        if (_projectId != null) {
            return _projectId;
        }
        String projectId = _utils.stripParentDirs(projectDir).trim().replaceAll(" ", "_");
        if (projectId.length() > 24) {
            //XNAT Project Titles are only 24 characters long
            projectId = projectId.substring(0, 9);
            Date now = new Date();
            String pattern = "yyyddMMHHmmss";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(now);
            projectId += "_" + date;
        }
        return projectId;
    }

    private void doImport(String projectJson, String projectDir, String projectId) {
        // Import BIDS project
        Date startTime = new Date();

        System.out.println("Importing " + projectDir + " to XNAT Project " + projectId);
        ImportObserver observer = new ImportObserver(projectId, _logPath);
        observer.beginSync(startTime);

        ImportedItem projectImportedItem = new ImportedItem(projectId, projectId);
        projectImportedItem.setXsiType(XnatProjectdataBean.SCHEMA_ELEMENT_NAME);
        projectImportedItem.setSyncTime(startTime);
        projectImportedItem.addObserver(observer);

        try {
            List<String> projectFiles;
            synchronized (_allFiles) {
                projectFiles = _allFiles.stream().filter(s -> s.startsWith(projectDir))
                        .collect(Collectors.toList());
            }

            XnatImporter importer = new XnatImporter(_manager, _connection, observer, _filesystemService, _utils,
                    projectImportedItem, projectId, projectDir, projectJson, projectFiles);
            importer.doImport();
            if (observer.isSuccess()) {
                projectImportedItem.setSyncStatus("IMPORTED");
            } else {
                noteError(projectImportedItem, "Import failed, review StdErr for cause");
            }
        } catch (Exception e) {
            String errMsg = "Error importing project " + projectId + ": " + e.getMessage();
            observer.error(errMsg, e);
            noteError(projectImportedItem, errMsg);
        } finally {
            projectImportedItem.stateChanged();
            observer.endSync(true);
            observer.close();
        }
    }

    private void noteError(ImportedItem projectImportedItem, String errMsg) {
        projectImportedItem.setSyncStatus("IMPORT_FAILED");
        projectImportedItem.setMessage(StringUtils.joinWith("\n", errMsg,
                StringUtils.defaultIfBlank(projectImportedItem.getMessage(), "")));

        rtnCode.set(1);
    }

    public void importData() throws Exception {
        List<String> projectJsons = _allFiles.stream()
                .filter(s -> _utils.fileEndsWith(s, BIDSConstants.DATASET_DESCRIPTION_JSON))
                .collect(Collectors.toList());

        // Check for valid inputs
        if (projectJsons.size() == 0) {
            throw new Exception("No BIDS project directories (marked by presence of " +
                    BIDSConstants.DATASET_DESCRIPTION_JSON + " file) exist within " + _rootFolderPath);
        } else if (projectJsons.size() > 1 && _projectId != null) {
            throw new Exception("Cannot process multiple BIDS project directories into a single XNAT project. " +
                    "Please rerun without specifying a project ID.");
        }

        final Map<String, String[]> projectMap = new HashMap<>();
        for (String projectJson : projectJsons) {
            final String projectDir = _utils.stripFilename(projectJson);
            final String projectId  = makeProjectId(projectDir);

            // Test that XNAT's filesystem service can access the _rootFolderPath files
            if (_externalFilesystem && !_manager.checkAccess(_connection, projectJson, projectId)) {
                throw new Exception("XNAT cannot access files in " + _rootFolderPath + ". In order to add them to your " +
                        "project, you'll need to work with the XNAT site's administrator to configure the filesystems " +
                        "plugin with a valid config for your S3 bucket and permit any necessary projects.");
            }
            projectMap.put(projectJson, new String[]{projectDir, projectId});
        }

        projectJsons.parallelStream().forEach(j -> {
            String[] projectData = projectMap.get(j);
            doImport(j, projectData[0], projectData[1]);
        });

        // Can't throw exception during parallel processing
        if (rtnCode.get() != 0) {
            throw new Exception("Failed to import one or more projects");
        }
    }

}
