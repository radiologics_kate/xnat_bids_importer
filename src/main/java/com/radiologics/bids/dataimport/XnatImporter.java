package com.radiologics.bids.dataimport;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.radiologics.bids.connection.RemoteConnection;
import com.radiologics.bids.connection.RemoteConnectionManager;
import com.radiologics.bids.connection.RemoteConnectionResponse;
import com.radiologics.bids.manifest.ImportedItem;
import com.radiologics.bids.observer.ImportObserver;
import com.radiologics.bids.utils.BIDSConstants;
import com.radiologics.bids.utils.BIDSFileUtils;
import com.radiologics.bids.utils.FileZipUtils;
import com.radiologics.filesystems.services.FilesystemService;
import com.radiologics.filesystems.services.LocalFilesystemService;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.nrg.xdat.bean.*;
import org.nrg.xdat.bean.base.BaseElement;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Mohana Ramaratnam
 */
public class XnatImporter {

    private final RemoteConnectionManager _manager;
    private final RemoteConnection _connection;
    private final ImportObserver _observer;
    private final FilesystemService _filesystemService;
    private final BIDSFileUtils _utils;
    private final String _projectId;
    private final ImportedItem _projectImportedItem;
    private String _projectDir;
    private String _dataDescriptionJSON;
    private String _participantTSV;
    private String _participantJson;
    private List<String> _projectFiles;
    private List<String> _projectResources;
    private List<String> _subjectFolders;
    private String _derivativesFolder;
    private String _codeFolder;

    public XnatImporter(final RemoteConnectionManager manager,
                        final RemoteConnection connection,
                        final ImportObserver observer,
                        final FilesystemService filesystemService,
                        final BIDSFileUtils utils,
                        final ImportedItem projectImportedItem,
                        String projectId, String projectDir, String dataDescriptionJSON, List<String> projectFiles) {
        _manager = manager;
        _connection = connection;
        _observer = observer;
        _filesystemService = filesystemService;
        _utils = utils;
        _projectId = projectId;
        _projectImportedItem = projectImportedItem;
        _projectDir = projectDir;
        _dataDescriptionJSON = dataDescriptionJSON;
        _projectFiles = projectFiles;
        _projectResources = new ArrayList<>();
        _subjectFolders = new ArrayList<>();
        _derivativesFolder = null;
        _codeFolder = null;
    }

    public void addResource(String rscString) {
        if (rscString != null) _projectResources.add(rscString);
    }

    public void addSubjectFolder(String s) {
        _subjectFolders.add(s);
    }

    public void setSubjectFolders(List<String> sFs) {
        _subjectFolders.addAll(sFs);
    }

    public void setDerivativesFolder(String derivative) {
        this._derivativesFolder = derivative;
    }

    public String getDerivativesFolder() {
        return _derivativesFolder;
    }

    public void setCodeFolder(String codeFolder) {
        this._codeFolder = codeFolder;
    }

    public String getCodeFolder() {
        return _codeFolder;
    }


    public void setParticipantTSV(String participantTSVString) {
        _participantTSV = participantTSVString;
    }

    public void setParticipantJson(String participantJsonString) {
        _participantJson = participantJsonString;
    }

    private void scanProjectDir() {
        // Add all files outside of subject directories as project resources
        _projectFiles.stream().filter(s -> !_utils.isDir(s) &&
                !_utils.withinSubjectDir(s))
                .forEach(this::addResource);

        // Add Subject Folders
        List<String> subFolders = _projectFiles.stream().filter(_utils::representsSubjectDir)
                .collect(Collectors.toList());
        setSubjectFolders(subFolders);

        // Participant files
        setParticipantTSV(_projectFiles.stream().filter(s -> _utils.fileEndsWith(s,
                BIDSConstants.PARTICIPANTS_TSV_FILENAME)).findFirst().orElse(null));
        setParticipantJson(_projectFiles.stream().filter(s -> _utils.fileEndsWith(s,
                BIDSConstants.PARTICIPANTS_JSON_FILENAME)).findFirst().orElse(null));

        // Addl files & dirs
        setDerivativesFolder(_projectFiles.stream().filter(s -> _utils.dirEndsWith(s,
                BIDSConstants.DERIVATIVES_FOLDER_NAME)).findFirst().orElse(null));
        // Files within code folders included as project resources for now
        //setCodeFolder(_projectFiles.filter(s -> _utils.dirEndsWith(s,
        //      BIDSConstants.CODE_FOLDER_NAME)).findFirst().orElse(null));
    }

    public void doImport() throws Exception {
        if (_dataDescriptionJSON == null) {
            // Shouldn't happen since this is how we ID projects to begin with
            throw new Exception("No " + BIDSConstants.DATASET_DESCRIPTION_JSON);
        }

        // Scan dir and set vars
        scanProjectDir();

        // Import project, will throw exception upon failure
        XnatProjectdataBean project = importProject();

        // Import all the project resources
        try {
            importProjectResources(project);
            _observer.updateManifest("Successfully imported project resources " +
                    _projectResources.stream().map(_utils::stripParentDirs).collect(Collectors.joining(",")));
        } catch (Exception e) {
            String errMsg = "Error importing project resources for " + _projectId + ": " + e.getMessage();
            _observer.updateManifest(errMsg, true);
            _observer.error(errMsg, e);
        }

        // Import subjects from TSV
        if (_participantTSV != null) {
            importFromTsv(_participantTSV, BIDSConstants.PARTICIPANT_ID,
                    properties -> importSubject(new SubjectParams(_observer, properties)));
        }

        // Import subjects, experiments, and scans
        for (String subjectFolder : _subjectFolders) {
            String subjectLabel = _utils.extractSubjectLabel(subjectFolder);
            if (StringUtils.isEmpty(subjectLabel)) {
                System.err.println("Invalid subject directory " + subjectFolder);
                return;
            }

            // Get subject ID (also will import any subject not imported from the tsv parsing)
            final String subjectAccessionId = importSubject(new SubjectParams(_observer, subjectLabel));
            if (subjectAccessionId == null) {
                //Unable to import subject, already logged the error, don't continue to import its experiments
                return;
            }

            // Sessions import from tsv
            String subjectSessionsTSV = _utils.getBIDSSessionTsv(subjectFolder);
            if (_projectFiles.stream().anyMatch(s -> s.equals(subjectSessionsTSV))) {
                importFromTsv(subjectSessionsTSV, BIDSConstants.SESSION_ID,
                        properties -> importMrSession(new SessionParams(_observer, properties),
                                subjectAccessionId, subjectLabel, subjectFolder, true));
            }

            // Sessions import from filepaths
            List<String> sessionDirs = _projectFiles.stream().filter(s ->
                    _utils.containsDir(s, subjectFolder) && _utils.representsSessionDir(s))
                    .collect(Collectors.toList());
            if (sessionDirs.size() > 0) {
                //Longitudinal style
                for (String sesDir : sessionDirs) {
                    importMrSession(new SessionParams(_observer, _utils.extractSessionLabel(sesDir)),
                            subjectAccessionId, subjectLabel, sesDir, false);
                }
            } else {
                //Cross-sectional (single session), give session the same label as the subject
                importMrSession(new SessionParams(_observer, subjectLabel), subjectAccessionId, subjectLabel,
                        subjectFolder, false);
            }
        }
    }

    /**
     * Do project import
     * @return the project bean
     * @throws Exception if import fails
     */
    private XnatProjectdataBean importProject() throws Exception {
        XnatProjectdataBean project = new XnatProjectdataBean();
        project.setId(_projectId);
        project.setSecondaryId(_projectId);
        project.setName(_projectId);
        setProjectDescriptionFromDatasetJson(project);

        // Experiment protocol
        XnatDatatypeprotocolBean dataTypeProtocol = new XnatDatatypeprotocolBean();
        dataTypeProtocol.setId(_projectId + "_xnat_mrSessionData");
        dataTypeProtocol.setName("MR Sessions");
        dataTypeProtocol.setDataType("xnat:mrSessionData");
        XnatFielddefinitiongroupBean defaultDefinition = new XnatFielddefinitiongroupBean();
        defaultDefinition.setId("default");
        defaultDefinition.setDataType("xnat:mrSessionData");
        defaultDefinition.setProjectSpecific("0");
        dataTypeProtocol.addDefinitions_definition(defaultDefinition);
        project.addStudyprotocol(dataTypeProtocol);

        // Subject protocol
        dataTypeProtocol = new XnatDatatypeprotocolBean();
        dataTypeProtocol.setId(_projectId + "_xnat_subjectData");
        dataTypeProtocol.setName("Subjects");
        dataTypeProtocol.setDataType("xnat:subjectData");
        defaultDefinition = new XnatFielddefinitiongroupBean();
        defaultDefinition.setId("default");
        defaultDefinition.setDataType("xnat:subjectData");
        defaultDefinition.setProjectSpecific("0");
        dataTypeProtocol.addDefinitions_definition(defaultDefinition);
        if (_participantJson != null) {
            //Create subject custom variable set from participants json
            createCustomSubjectVariablesFromParticipantJson(dataTypeProtocol);
        }
        project.addStudyprotocol(dataTypeProtocol);

        // Import the project in XNAT, throw an exception on failure since we cannot continue
        try {
            _manager.importProject(_connection, project);
        } catch (Exception e) {
            // Need to catch the exception here where we have the project bean
            String xmlFile = _observer.saveFailedUpload(project, _projectId);
            _projectImportedItem.setMessage("See XML " + xmlFile);
            _projectImportedItem.setException(e);
            throw e;
        }
        return project;
    }

    /**
     * Parse _dataDescriptionJSON for project description in XNAT
     * @param project the project bean
     */
    private void setProjectDescriptionFromDatasetJson(XnatProjectdataBean project) {
        // Parse dataset description json
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode rootNode;
            try(InputStream is = _filesystemService.getInputStream(_dataDescriptionJSON, project.getId())) {
                rootNode = objectMapper.readTree(is);
            }
            StringBuilder desc = new StringBuilder();
            for (String key : BIDSConstants.DataDescriptionKeys) {
                JsonNode node = rootNode.get(key);
                if (node != null) {
                    desc.append(key).append("=").append(node.toString()).append("\n");
                }
            }
            String description = desc.toString();
            if (StringUtils.isNotEmpty(description)) {
                project.setDescription(description);
            }
        } catch (Exception e) {
            _observer.error("Error parsing " + _dataDescriptionJSON + ": " + e.getMessage(), e);
        }
    }

    /**
     * Parse _participantJson to create custom subject-level variables in XNAT
     * @param dataTypeProtocol the subject datatype protocol bean
     */
    private void createCustomSubjectVariablesFromParticipantJson(XnatDatatypeprotocolBean dataTypeProtocol) {
        XnatFielddefinitiongroupBean bidsDefinition = new XnatFielddefinitiongroupBean();
        bidsDefinition.setId("BIDS_" + _projectId);
        bidsDefinition.setDescription("BIDS Subject Fields from " + _participantJson);
        bidsDefinition.setDataType("xnat:subjectData");
        bidsDefinition.setProjectSpecific("1");

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode rootNode;
            try(InputStream is = _filesystemService.getInputStream(_participantJson, _projectId)) {
                rootNode = objectMapper.readTree(is);
            }
            if (rootNode != null) {
                Iterator<String> fields = rootNode.fieldNames();
                while (fields.hasNext()) {
                    String f = fields.next();
                    JsonNode node = rootNode.get(f);
                    if (node != null) {
                        XnatFielddefinitiongroupFieldBean singleField = new XnatFielddefinitiongroupFieldBean();
                        singleField.setName(f);
                        singleField.setType("custom");
                        singleField.setDatatype("string");
                        singleField.setRequired("0");
                        singleField.setXmlpath("xnat:subjectData/fields/field[name=" + f + "]/field");
                        JsonNode levelsNode = node.get("Levels");
                        if (levelsNode != null) {
                            Iterator<String> levelsFields = levelsNode.fieldNames();
                            while (levelsFields.hasNext()) {
                                String possibleValue = levelsFields.next();
                                XnatFielddefinitiongroupFieldPossiblevalueBean pValueBean = new XnatFielddefinitiongroupFieldPossiblevalueBean();
                                pValueBean.setPossiblevalue(possibleValue);
                                singleField.addPossiblevalues_possiblevalue(pValueBean);
                            }
                        }
                        bidsDefinition.addFields_field(singleField);
                    }

                }
            }
        } catch (Exception e) {
            _observer.error("Error parsing " + _participantJson + ": " + e.getMessage(), e);
        }
        dataTypeProtocol.addDefinitions_definition(bidsDefinition);
    }

    /**
     * Make zipfile
     * @param filelist list of filepaths to include
     * @param rootDir directory in which to store the zip
     * @return the zipfile
     * @throws Exception if unable to write zip
     */
    private File makeZip(List<String> filelist, String rootDir, boolean includeRelPaths) throws Exception {
        FileZipUtils FileZipUtils = new FileZipUtils();
        File zipfile = FileZipUtils.buildZip(filelist, rootDir, includeRelPaths, _observer);
        if (zipfile != null && zipfile.exists()) {
            return zipfile;
        } else {
            return null;
        }
    }

    /**
     * Get payload for resource upload (either list of strings or zipfile)
     * @param resources the resources to collection
     * @throws Exception for issues
     */
    private Object getPayload(List<String> resources, String rootDir) throws Exception {
        return getPayload(resources, rootDir, false);
    }

    /**
     * Get payload for resource upload (either list of strings or zipfile)
     * @param resources the resources to collection
     * @param includeRelativePaths whether to include relative paths in zipfile
     * @throws Exception for issues
     */
    private Object getPayload(List<String> resources, String rootDir, boolean includeRelativePaths) throws Exception {
        Object payload;
        if (_filesystemService instanceof LocalFilesystemService) {
            //Create a zip file of resources
            payload = makeZip(resources, rootDir, includeRelativePaths);
        } else {
            // Make json list of resources
            JSONObject resourceMap = new JSONObject();
            for (String resource : resources) {
                String relPath = (includeRelativePaths) ?
                        resource.replace(rootDir, "") :
                        _utils.stripParentDirs(resource);

                resourceMap.put(resource, relPath);
            }
            payload = resourceMap.toString();
        }
        return payload;
    }

    /**
     * Import elements into XNAT based on tsv
     * @param tsv the tsv filename
     * @param requiredCol the column name required by BIDS for valid tsv at this level
     * @param importer a function to do the importing (will vary by element)
     */
    private void importFromTsv(String tsv, String requiredCol, Consumer<Properties> importer) {
        String[] columnHeaders = null;
        try (BufferedReader bReader = new BufferedReader(new InputStreamReader(_filesystemService.getInputStream(tsv, _projectId)))) {
            String line;
            //Looping the read block until all lines are read
            boolean validColumns = false;
            while ((line = bReader.readLine()) != null) {
                //Splitting the content of tabbed separated line
                String[] datavalue = line.split("\\t");
                if (!validColumns) {
                    if (datavalue.length > 0) {
                        columnHeaders = datavalue;
                        for (String col : columnHeaders) {
                            if (col.equals(requiredCol)) {
                                validColumns = true;
                                break;
                            }
                        }
                    }
                    if (!validColumns) {
                        break;
                    }
                } else {
                    if (datavalue.length == 0) {
                        //invalid line?
                        continue;
                    }
                    Properties prop = new Properties();
                    for (int i = 0; i < datavalue.length; i++) {
                        prop.setProperty(columnHeaders[i], datavalue[i]);
                    }
                    // Call the importer for this line of properties (one line per element)
                    try {
                        importer.accept(prop);
                    } catch (RuntimeException e) {
                        // We shouldn't see an exception here as it should be handled within the importer, but if we do,
                        // we don't want to stop parsing the rest of the data
                        _observer.error(e.getMessage(), e);
                    }
                }
            }
        } catch (Exception e) {
            _observer.error("Exception while reading " + tsv + ": " + e.getMessage(), e);
        }
    }

    /**
     * Import element into XNAT, return ID if already imported
     * @param element the element
     * @param label the element label in xnat
     * @param importer the importer function
     * @return the element ID in XNAT
     */
    private String importElement(BaseElement element, String label,
                         Function<BaseElement, RemoteConnectionResponse> importer) {
        String type = element.getXSIType();
        String id = _observer.getXnatId(label, type);
        if (id != null) {
            // Already created
            return id;
        }

        System.out.println("Importing " + type + " " + label);
        ImportedItem importItem = new ImportedItem(null, label);
        importItem.setXsiType(type);
        importItem.setSyncTime(new Date());
        importItem.addObserver(_observer);
        RemoteConnectionResponse response;
        try {
            response = importer.apply(element);
        } catch (Exception e) {
            String xmlFile = _observer.saveFailedUpload(element, label);
            importItem.setSyncStatus("IMPORT_FAILED");
            importItem.setMessage("See XML " + xmlFile);
            importItem.setException(e);
            importItem.stateChanged();
            _observer.error("Error importing " + type + " " + label + ": " + e.getMessage(), e);
            return null;
        }
        id = response.getResponseBody();
        importItem.setLocalId(id);
        _observer.saveXnatId(label, id, type);
        importItem.setSyncStatus("IMPORTED");
        importItem.stateChanged();
        return id;
    }

    /**
     * Import project-level resources to XNAT
     * @param project the project bean
     * @throws Exception for issues importing or if no payload
     */
    private void importProjectResources(XnatProjectdataBean project) throws Exception {
        Object payload = getPayload(_projectResources, _projectDir, true);
        if (payload != null) {
            _manager.importProjectResource(_connection, project.getId(),"BIDS", payload);
        } else {
            throw new Exception("No payload for project resource import");
        }
    }

    /**
     * Import subject
     * @param subjectParams the participant params from tsv
     * @return the imported subject's ID in XNAT
     */
    private String importSubject(SubjectParams subjectParams) {
        XnatSubjectdataBean subject = subjectParams.toSubjectBean(_projectId);
        String subjectLabel = subject.getLabel();
        return importElement(subject, subjectLabel,
                s -> _manager.importSubject(_connection, (XnatSubjectdataBean) s));
    }

    /**
     * Import MR session, including scans and non-scan resources (like behavioral files)
     * @param sessionParams the session params from tsv
     * @param subjectId the corresponding subject ID
     * @param subjectLabel the corresponding subject label
     * @param baseDir the session directory (or subject directory if append=T)
     * @param append T/F if True, may append ses-visitId to dir, otherwise, assume dir is the session dir
     * @return the imported session's ID
     */
    private String importMrSession(SessionParams sessionParams, String subjectId, String subjectLabel,
                                   String baseDir, boolean append) {
        List<String> subFiles = null;
        XnatMrsessiondataBean session = sessionParams.toXnatMrSessionDataBean(_projectId, subjectId, subjectLabel);
        String label = session.getLabel();
        String project = session.getProject();

        String id = _observer.getXnatId(label, session.getXSIType());
        if (id != null) {
            // Already imported
            return id;
        }

        String visitId = session.getVisitId();
        String sessionDir = baseDir;
        if (append) {
            sessionDir += _utils.getBIDSSessionDir(visitId);
            // Files that are associated with the subject but not any particular session
            subFiles = _projectFiles.stream().filter(d -> _utils.startsWithDir(d, baseDir)
                            && !_utils.withinSessionDir(d) && _utils.isFile(d)
                    ).collect(Collectors.toList());

        }
        final String sesDir = sessionDir;

        // Collect session files & dirs
        List<String> sesFilesAndDirs = _projectFiles.stream().filter(d -> _utils.startsWithDir(d, sesDir) &&
                !d.equals(sesDir)).collect(Collectors.toList());
        List<String> sesDerivFilesAndDirs = null;
        if (StringUtils.isNotBlank(_derivativesFolder)) {
            final String subjSesBidsDir = sessionDir.replaceAll("^" + _projectDir, "");
            sesDerivFilesAndDirs = _projectFiles.stream().filter(d ->
                    _utils.startsWithDir(d, _derivativesFolder, subjSesBidsDir)).collect(Collectors.toList());
        }

        // Is there a scan tsv?
        final String scanTsv = _utils.getBIDSScanTsv(sesDir);
        final Map<String, Properties> scanProperties;
        if (sesFilesAndDirs.stream().anyMatch(s -> s.equals(scanTsv))) {
            scanProperties = new HashMap<>();
            importFromTsv(scanTsv, BIDSConstants.SCAN_ID,
                    properties -> scanProperties.put(properties.getProperty(BIDSConstants.SCAN_ID), properties));
        } else {
            scanProperties = null;
        }

        // Create XnatMrscandataBeans and gather scan resources
        ScanExtractor scanExtractor = new ScanExtractor(_filesystemService, _utils, _observer, sesDir,
                sesFilesAndDirs, scanProperties, subFiles, sesDerivFilesAndDirs,
                _projectResources);
        Hashtable<XnatMrscandataBean, Hashtable<String, List<String>>> scans = scanExtractor.getScans();

        // Add scans to session for import
        for (XnatMrscandataBean scan : scans.keySet()) {
            scan.setImageSessionId(label);
            scan.setProject(project);
            session.addScans_scan(scan);
        }

        // Import session
        id = importElement(session, label,
                s -> _manager.importImageSession(_connection, (XnatImagesessiondataBean) s));

        if (id == null) {
            // Issue with import
            return null;
        }

        // Import all the scan resource files for each scan
        importScanResources(scans, sesDir, session);

        //Import behavioral files
        //These are files within subdirectories that don't contain any nifti files
        List<String> niftiDirs = sesFilesAndDirs.stream().filter(_utils::isNifti).map(_utils::stripFilename)
                .distinct().collect(Collectors.toList());
        sesFilesAndDirs.stream().filter(_utils::isDir).filter(f -> !niftiDirs.contains(f)).forEach(dir -> {
            String behType =  _utils.stripParentDirs(dir);
            List<String> behFiles = sesFilesAndDirs.stream().filter(
                    f -> _utils.startsWithDir(f, dir) && _utils.isFile(f)).collect(Collectors.toList());
            String suffix = "/resources/" + behType;
            String message = " Resource: " + behType;
            importSessionResource(behFiles, session, sesDir, suffix, message);

        });

        return id;
    }

    /**
     * Import scan resources into XNAT
     * @param scans the scan hashtable returned from scanExtractor
     * @param sesDir the corresponding session directory
     * @param session the corresponding session bean
     */
    private void importScanResources(Hashtable<XnatMrscandataBean, Hashtable<String, List<String>>> scans, String sesDir,
                             XnatMrsessiondataBean session){
        for (XnatMrscandataBean scan : scans.keySet()) {
            Hashtable<String, List<String>> scanFileHash = scans.get(scan);
            if (scanFileHash.size() == 0) {
                continue;
            }
            for (String resource : scanFileHash.keySet()) {
                String format = resource;
                String content = resource;
                if (resource.equals(BIDSConstants.NIFTI_RESOURCE)) {
                    format = BIDSConstants.NIFTI_RESOURCE + "_RAW";
                    content = BIDSConstants.NIFTI_RESOURCE + "_RAW";
                }
                List<String> scanFiles = scanFileHash.get(resource);
                if (scanFiles.size() == 0) {
                    continue;
                }
                String suffix = "/scans/" + scan.getId() + "/resources/" + resource;
                String message = " Scan: " + scan.getId() + " Resource: " + resource;
                importSessionResource(scanFiles, session, sesDir, suffix, message, format, content);
            }
        }
    }

    /**
     * Import session resources into XNAT
     * @param files a list of resource filepaths
     * @param session the session bean
     * @param sesDir the session directory
     * @param suffix the URL suffix for uploading
     * @param message message for logging
     */
    private void importSessionResource(List<String> files, XnatMrsessiondataBean session, String sesDir, String suffix,
                                       String message) {
        importSessionResource(files, session, sesDir, suffix, message, null, null);
    }

    /**
     * Import session resources into XNAT
     * @param files a list of resource filepaths
     * @param session the session bean
     * @param sesDir the session directory
     * @param suffix the URL suffix for uploading
     * @param message message for logging
     * @param format format argument to be appended to URL
     * @param content content argument to be appended to URL
     */
    private void importSessionResource(List<String> files, XnatMrsessiondataBean session, String sesDir, String suffix,
                                       String message, String format, String content) {
        String label = session.getLabel();
        try {
            Object payload = getPayload(files, sesDir);
            String urlTag = "";
            if (payload instanceof File) {
                //Create a zip file of resources
                urlTag = "/files?overwrite=true&extract=true&tags=BIDS";
                if (StringUtils.isNotBlank(format)) {
                    urlTag += "&format=" + format;
                }
                if (StringUtils.isNotBlank(content)) {
                    urlTag += "&content=" + content;
                }
            }
            if (payload == null) {
                return;
            }
            String url_postfix = "/data/archive/projects/" + _projectId +
                    "/subjects/" + session.getSubjectId() + "/experiments/" + label + suffix + urlTag;

            _manager.importResource(_connection, url_postfix, payload);
            _observer.updateManifest("Resource import succeeded for MR: " +
                    label + " " + message);
        } catch (Exception e) {
            String errMsg = "Resource import FAILED for MR: " +
                    label + " " + message + ": " + e.getMessage();
            _observer.updateManifest(errMsg, true);
            _observer.error(errMsg, e);
        }
    }

}
