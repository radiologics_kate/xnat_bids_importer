package com.radiologics.bids.importer;

import com.radiologics.bids.configuration.BidsDataImporterConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import picocli.CommandLine.ParameterException;

public class BidsDataImporterApp {

    public static void main(String[] args) {
        int exitCode = 0;
        AbstractApplicationContext context = new AnnotationConfigApplicationContext(BidsDataImporterConfiguration.class);
        BidsDataImporter p = context.getBean(BidsDataImporter.class);
        try {
            p.importData(args);
        } catch (ParameterException e) {
            //Don't print stack trace for param exception
            exitCode = 1;
        } catch (Exception e) {
            System.err.println("ERROR:");
            e.printStackTrace();
            exitCode = 1;
        }
        context.close();
        System.exit(exitCode);
    }
}
