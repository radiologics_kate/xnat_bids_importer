package com.radiologics.bids.services;


import com.radiologics.bids.connection.RemoteConnection;
import com.radiologics.bids.connection.RemoteConnectionManager;
import com.radiologics.bids.connection.RemoteConnectionResponse;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatProjectdataBean;
import org.nrg.xdat.bean.XnatSubjectdataBean;
import org.nrg.xdat.bean.base.BaseElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;

import javax.annotation.Nullable;
import java.io.File;
import java.io.StringWriter;
import java.util.Map;


/**
 * The Class RemoteRESTServiceImpl.
 */
@Service("remoteRESTService")
public class RemoteRESTServiceImpl extends AbstractRemoteRESTService implements RemoteRESTService {

    // TODO:  Do we want this to be configurable?
    public static final int TRUNCATE_LOG_OUTPUT_LENGTH = 1000;

    private long sleep = 10;
    private int maxTries = 5;


    @Autowired
    public RemoteRESTServiceImpl() {}

    /**
     * Make xml for element
     * @param element the element
     * @return the xml string
     * @throws Exception if issue
     */
    private String makeXml(BaseElement element) throws Exception {
        String xml;
        try(StringWriter tsw = new StringWriter()) {
            element.toXML(tsw);
            xml = tsw.toString();
        }
        return xml;
    }

    /**
     * Import the element
     * @param connection the connection
     * @param element the element
     * @param uri the import uri
     * @return the response
     * @throws RuntimeException if issues
     */
    private RemoteConnectionResponse importElement(RemoteConnection connection, BaseElement element,
                                                   String uri)
            throws RuntimeException {
        //do we need the assessor data and how.
        //MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();

        // NOTE: Just call toXML on the subject object here, rather than calling getItem on the subject object and obtaining
        // XML from the item.  Using getItem() will query for experiments and assessors associated with the assession number.
        // In the case where there is a source-side subject with the same assession number as the destination-side subject,
        // this could result in the wrong sessions being sent to the destination, potentially overwriting previously synced
        // sessions with sessions from a subject outside the source project.
        //log.debug("URL: " + uri);
        RemoteConnectionResponse response;
        try {
            response = doRequest(connection, uri, HttpMethod.PUT, makeXml(element));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return response;
    }

    /**
     * Check access to path uri
     * @param connection the connection
     * @param uri the path uri
     * @param project the project
     * @return the response
     * @throws RuntimeException for issues
     */
    @Override
    public RemoteConnectionResponse checkAccess(RemoteConnection connection, String uri, String project) throws RuntimeException {
        RemoteConnectionResponse response;
        try {
            response = doRequest(connection, connection.getUrl() +
                    "/xapi/remote_files/check_access?url=" + uri + "&project=" + project,
                    HttpMethod.GET);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return response;
    }

    /**
     * Import project with retry.
     *
     * @param connection the connection
     * @param project    the project
     * @return response
     * @throws RuntimeException the runtime exception
     */
    public RemoteConnectionResponse importProject(RemoteConnection connection, XnatProjectdataBean project)
            throws RuntimeException {
        String projectId = project.getId();
        String uri = connection.getUrl() + "/data/archive/projects/" + projectId + "?inbody=true";
        return importElement(connection, project, uri);
    }

    /**
     * Import ImageSession with retry.
     *
     * @param connection the connection
     * @param experiment the experiment
     * @return response
     * @throws RuntimeException the runtime exception
     */
    public RemoteConnectionResponse importImageSession(RemoteConnection connection, XnatImagesessiondataBean experiment)
            throws RuntimeException {
        String uri = connection.getUrl() + "/data/archive/projects/" + experiment.getProject() + "/subjects/" +
                experiment.getSubjectId() + "/experiments/" + experiment.getLabel() + "?inbody=true";
        return importElement(connection, experiment, uri);
    }

    /**
     * Import Subject with retry.
     *
     * @param connection the connection
     * @param subject    the subject
     * @return response
     * @throws RuntimeException the runtime exception
     */
    public RemoteConnectionResponse importSubject(RemoteConnection connection, XnatSubjectdataBean subject)
            throws RuntimeException {
        String uri = connection.getUrl() + "/data/archive/projects/" + subject.getProject() +
                "/subjects/" + subject.getLabel() + "?inbody=true";
        return importElement(connection, subject, uri);
    }

    /**
     * import Subject Resource with retry.
     *
     * @param connection the connection
     * @param subject  the subject
     * @param resourceLabel the resource label
     * @param zipFile a zipfile of contents
     * @return response
     * @throws RuntimeException the runtime exception
     */
    public RemoteConnectionResponse importSubjectResource(RemoteConnection connection, XnatSubjectdataBean subject,
                                                          String resourceLabel, File zipFile) throws Exception {
        String urlSuffix = "/data/archive/projects/" + subject.getProject() + "/subjects/" + subject.getId() +
                "/resources/" + resourceLabel + "/files?overwrite=true&extract=true";
        return importResource(connection, urlSuffix, zipFile);
    }

    /**
     * import Project Resource with retry.
     *
     * @param connection the connection
     * @param projectId  the Project ID
     * @param resourceLabel the resource label
     * @param zipFile a zipfile of contents
     * @return response
     * @throws RuntimeException the runtime exception
     */
    public RemoteConnectionResponse importProjectResource(RemoteConnection connection, String projectId,
                                                          String resourceLabel, File zipFile) throws RuntimeException {
        String urlSuffix = "/data/archive/projects/" + projectId + "/resources/" +
                resourceLabel + "/files?overwrite=true&extract=true";
        return importResource(connection, urlSuffix, zipFile);
    }

    /**
     * import Project Resource with retry.
     *
     * @param connection the connection
     * @param projectId  the Project ID
     * @param resourceLabel the resource label
     * @param urlJson a list of urls to add to the resource
     * @return response
     * @throws RuntimeException the runtime exception
     */
    public RemoteConnectionResponse importProjectResource(RemoteConnection connection, String projectId,
                                                          String resourceLabel, String urlJson) throws RuntimeException {
        String urlSuffix = "/data/archive/projects/" + projectId + "/resources/" + resourceLabel;
        return importResource(connection, urlSuffix, urlJson);
    }

    /**
     * import Resource with retry.
     *
     * @param connection the connection
     * @param url_postfix suffix for connection.getUrl()
     * @param zipFile the zipfile payload
     * @return response
     * @throws RuntimeException the runtime exception
     */
    public RemoteConnectionResponse importResource(RemoteConnection connection, String url_postfix, File zipFile)
            throws RuntimeException {
        String uri = connection.getUrl() + url_postfix;
        return this.importZip(connection, uri, zipFile);
    }

    /**
     * import Resource with retry.
     *
     * @param connection the connection
     * @param url_postfix suffix for connection.getUrl()
     * @param urlJson json string of urls
     * @return response
     * @throws RuntimeException for issues
     */
    public RemoteConnectionResponse importResource(RemoteConnection connection, String url_postfix, String urlJson)
            throws RuntimeException {
        //Add the urls
        String uri = connection.getUrl() + "/xapi/remote_files/add_to_catalog?create=true&resource=" + url_postfix;
        return doRequest(connection, uri, HttpMethod.PUT, urlJson, MediaType.APPLICATION_JSON);
    }

    /**
     * Import Zip with retry
     *
     * @param connection the connection
     * @param zip        the zip
     * @return true, if successful
     * @throws RuntimeException the runtime exception
     */
    private RemoteConnectionResponse importZip(RemoteConnection connection, String uri, File zip) throws RuntimeException {
        //this.setAliasToken(connection);
        final MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
        body.add("field", "value");
        if (zip != null) body.add("file", new FileSystemResource(zip));
        return doRequest(connection, uri, HttpMethod.PUT, body);
    }

    /**
     * Get URI result.
     *
     * @param connection the connection
     * @param uri        the uri
     * @return ResponseEntity wrapper
     */
    public RemoteConnectionResponse getResult(RemoteConnection connection, String uri) throws RuntimeException {
        return doRequest(connection, uri, HttpMethod.GET);
    }

    /**
     * Make request
     *
     * @param connection the connection
     * @param uri        the uri
     * @param method     the http method
     *
     * @return ResponseEntity wrapper
     */
    public RemoteConnectionResponse doRequest(RemoteConnection connection, String uri, HttpMethod method) throws RuntimeException {
        return doRequest(connection, uri, method, null);
    }

    /**
     * Get HttpEntity based on body and whether or not to use and refresh JsessionId
     * @param connection connection
     * @param body request body
     * @param useJsessionId T/F use JsessionId
     * @param refreshJsessionId T/F refresh JsessionId
     * @param contentType the content type
     * @param <T> request body type
     * @return the HttpEntity
     */
    private <T> HttpEntity<T> getEntity(RemoteConnection connection, T body, boolean useJsessionId,
                                        boolean refreshJsessionId, @Nullable MediaType contentType) {
        HttpEntity<T> entity;
        HttpHeaders headers = RemoteConnectionManager.GetAuthHeaders(connection, useJsessionId, refreshJsessionId);
        if (body != null) {
            if (contentType != null) {
                headers.setContentType(contentType);
            }
            entity = new HttpEntity<>(body, headers);
        } else {
            entity = new HttpEntity<>(headers);
        }
        return entity;
    }

    /**
     * Either sleep and increment counter or throw exception
     * @param count counter
     * @param e exception
     * @return incremented counter
     * @throws RuntimeException if counter >= maxTries
     */
    private int checkRetry(int count, RuntimeException e) throws RuntimeException {
        try {
            //log.error("Retry count " + count);
            //if (response != null) log.debug(response);
            if (maxTries > 0) Thread.sleep(sleep);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        // if we've already tried maxTries times, throw exception
        if (maxTries == 0 || ++count >= maxTries) throw e;
        return count;
    }

    /**
     * Make request
     *
     * @param connection the connection
     * @param uri        the uri
     * @param method     the http method
     * @param body       the body
     *
     * @return ResponseEntity wrapper
     * @throws RuntimeException if error
     */
    public <T> RemoteConnectionResponse doRequest(RemoteConnection connection, String uri, HttpMethod method, T body)
            throws RuntimeException {
        return doRequest(connection, uri, method, body, null);
    }

    /**
     * Make request
     *
     * @param connection the connection
     * @param uri        the uri
     * @param method     the http method
     * @param body       the body
     * @param contentType the content type header
     * @return ResponseEntity wrapper
     * @throws RuntimeException if error
     */
    public <T> RemoteConnectionResponse doRequest(RemoteConnection connection, String uri, HttpMethod method, T body,
                                                  MediaType contentType)
            throws RuntimeException {
        HttpEntity<T> entity = getEntity(connection, body, true, false, contentType);

        int count = 0;
        while (true) {
            ResponseEntity<String> response;
            try {
                try {
                    response = getResttemplate().exchange(uri, method, entity, String.class);
                } catch (HttpClientErrorException e) {
                    if (e.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                        if (count == 0) {
                            // Refresh the Jsession
                            entity = getEntity(connection, body, true, true, contentType);
                        } else {
                            // Still not working, try with username/password
                            entity = getEntity(connection, body, false, false, contentType);
                        }
                        count = checkRetry(count, e);
                        continue;
                    } else if (e.getStatusCode() == HttpStatus.CONFLICT) {
                        // Ignore this exception
                        //log.debug("Resource already exists");
                        return null;
                    } else {
                        throw e;
                    }
                }
                //log.info(truncateStr(response));
                //log.info(truncateStr(response.getBody()));
                //log.info(truncateStr(response.getHeaders().get("Set-Cookie")));

                // RestTemplate.exchange already throws error for unsuccessful status, but maybe not all other statuses
                // are considered failures?
                boolean success = response.getStatusCode().value() == HttpStatus.OK.value() ||
                        response.getStatusCode().value() == HttpStatus.CREATED.value();
                if (!success) {
                    throw new RuntimeException("Unsuccessful response code: " + response.getStatusCode().toString() +
                            " " + response.getStatusCode().getReasonPhrase() +
                            ". Please confirm the credentials you are using have not expired.");
                }

                return new RemoteConnectionResponse(response);
            } catch (RuntimeException e) {
                count = checkRetry(count, e);
            }
        }
    }

    public String truncateStr(Object obj) {
        return truncateStr(obj, TRUNCATE_LOG_OUTPUT_LENGTH);
    }

    public String truncateStr(Object obj, int maxlength) {
        if (obj == null) {
            return null;
        }
        return (obj.toString().length() > TRUNCATE_LOG_OUTPUT_LENGTH) ? obj.toString().substring(0, TRUNCATE_LOG_OUTPUT_LENGTH) + "......." : obj.toString();
    }
}
