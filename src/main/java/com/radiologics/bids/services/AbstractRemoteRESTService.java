package com.radiologics.bids.services;

import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @author Mohana Ramaratnam
 */
public abstract class AbstractRemoteRESTService {
    /**
     * Gets the resttemplate.
     *
     * @return the resttemplate
     */
    public RestTemplate getResttemplate() {
        final SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setBufferRequestBody(false);
        final RestTemplate template = new RestTemplate(requestFactory);
        //template.setErrorHandler(new XsyncResponseErrorHandler());
        return template;
    }

}
