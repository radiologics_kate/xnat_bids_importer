package com.radiologics.bids.manifest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Hashtable;

/**
 * @author Mohana Ramaratnam
 */
public class ImportManifest {
    String projectId;
    Date sync_start_time;
    Date sync_end_time;
    String message;
    boolean wasSuccessful = true;

    public ImportManifest(String projectId) {
        this.projectId = projectId;
        this.message = "";
    }

    /**
     * @return the sync_start_time
     */
    public Date getSync_start_time() {
        return sync_start_time;
    }

    /**
     * @param sync_start_time the sync_start_time to set
     */
    public void setSync_start_time(Date sync_start_time) {
        this.sync_start_time = sync_start_time;
    }

    /**
     * @return the sync_end_time
     */
    public Date getSync_end_time() {
        return sync_end_time;
    }

    /**
     * @param sync_end_time the sync_end_time to set
     */
    public void setSync_end_time(Date sync_end_time) {
        this.sync_end_time = sync_end_time;
    }

    public void markIssue() {
        wasSuccessful = false;
    }

    /**
     * @return the localProjectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @param message the message to append
     */
    public void appendMessage(String message) {
        this.message += "<br/>" + message;
    }


    public boolean wasSyncSuccessfull() {
        return wasSuccessful;
    }


    public boolean shouldNotify() {
        boolean shouldNotify = false;
        return shouldNotify;
    }

    public void informUser() {
        //Placeholder?
        //final Hashtable<String, String> info = syncInfoAsHTML();
    }

    /**
     * Format sync information to requesting user.
     */
    public Hashtable<String, String> syncInfoAsHTML() {
        final Hashtable<String, String> info = new Hashtable<>();
        final String subject = "Project " + this.projectId;
        info.put("SUBJECT", subject);
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        sb.append("<body>");
        sb.append("<p>The following data  was imported into project ").append(projectId).append(". </p>");


        sb.append("<table>");
        sb.append("<tr>");
        sb.append("<th> Sync Start Time </th>");
        sb.append("<th> Sync End Time </th>");
        sb.append("</tr>");

        sb.append("<tr>");
        sb.append("<td>").append(this.getSync_start_time()).append("</td>");
        sb.append("<td>").append(this.getSync_end_time()).append("</td>");
        sb.append("</tr>");
        sb.append("</table>");

        sb.append("<p>").append(message).append("</p>");
        sb.append("</body>");
        sb.append("</html>");
        info.put("BODY", sb.toString());
        return info;
    }

    /**
     * Format sync information to requesting user.
     */
    public void syncInfoToFile(File file) {
        final Hashtable<String, String> info = syncInfoAsHTML();

        file.getParentFile().mkdirs();
        try (final BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(info.get("BODY"));
        } catch (IOException e) {
            System.err.println("An error occurred writing the sync file " + e.getMessage());
            e.printStackTrace();
        }
    }

}
