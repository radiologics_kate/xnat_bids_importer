package com.radiologics.bids.utils;

import com.radiologics.bids.observer.ImportObserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author Mohana Ramaratnam
 */
public class FileZipUtils {

    public File buildZip(List<String> filesInZip, String rootDir, boolean includeRelPaths, ImportObserver observer) throws Exception {
        if (filesInZip == null || filesInZip.size() == 0) {
            throw new Exception("No files to zip");
        }

        File cachePath = new File(observer.getProjectLogDir());
        if (!cachePath.isDirectory() && !cachePath.mkdirs()) {
            throw new Exception("Unable to create cache directory " + cachePath);
        }

        File zipFile = new File(cachePath , (new Date()).getTime() + ".zip");
        createZipFile(zipFile, filesInZip, rootDir, includeRelPaths);
        zipFile.deleteOnExit();
        return zipFile;
    }

    private void createZipFile(File zipFile, List<String> files, String rootDir, boolean includeRelPaths) throws IOException {
        URI base = new File(rootDir).toURI();
        // create byte buffer
        byte[] buffer = new byte[1024];
        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile))) {
            for (String s : files) {
                File f = new File(s);
                if (!f.isFile()) {
                    continue;
                }
                String relPath = (includeRelPaths) ? base.relativize(f.toURI()).getPath() : f.getName();
                FileInputStream fis = new FileInputStream(f);
                // begin writing a new ZIP entry, positions the stream to the start of the entry data
                zos.putNextEntry(new ZipEntry(relPath));
                int length;
                while ((length = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, length);
                }
                zos.closeEntry();
            }
        }
    }

}

