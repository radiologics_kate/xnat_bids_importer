package com.radiologics.bids.utils;

import org.apache.commons.lang3.StringUtils;
import org.nrg.xdat.bean.XnatMrsessiondataBean;
import org.nrg.xdat.bean.base.BaseElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.BiFunction;

/**
 * @author Mohana Ramaratnam
 */
public class BIDSConstants {

    public static final String NEWLINE = System.getProperty("line.separator");

    public final static String PARTICIPANTS_TSV_FILENAME = "participants.tsv";
    public final static String PARTICIPANTS_JSON_FILENAME = "participants.json";
    public final static String SESSIONS_TSV_SUFFIX = "_sessions.tsv";
    public final static String SCANS_TSV_SUFFIX = "_scans.tsv";

    public final static String DATASET_DESCRIPTION_JSON = "dataset_description.json";
    public final static String README = "README";
    public final static String CHANGES = "CHANGES";
    public final static String DERIVATIVES_FOLDER_NAME = "derivatives";
    public final static String CODE_FOLDER_NAME = "code";

    public final static String SUBJECT_PREFIX = "sub-";
    public final static String SESSION_PREFIX = "ses-";
    public final static String TASK_PREFIX = "task-";
    public final static String ACQ_PREFIX = "acq-";
    public final static String CE_PREFIX = "ce-";
    public final static String REC_PREFIX = "rec-";
    public final static String RUN_PREFIX = "run-";
    public final static String ECHO_PREFIX = "echo-";
    public final static String MOD_PREFIX = "mod-";
    public final static String MODALITY_LABEL = "MODALITY_LABEL";
    public final static String BOLD = "bold";
    public final static String SBREF = "sbref";

    public final static String JSON_EXT = ".json";
    public final static String NII_EXT = ".nii";
    public final static String NII_GZ_EXT = ".nii.gz";
    public final static String TSV_EXT = ".tsv";


    public final static String PARTICIPANT_ID = "participant_id";
    public final static String SESSION_ID = "session_id";
    public final static String SCAN_ID = "filename";
    public final static String SITE = "site";

    public final static String NIFTI_RESOURCE = "NIFTI";
    public final static String BIDS_RESOURCE = "BIDS";
    public final static String DERIVATIVES_RESOURCE = "derivatives";


    public static final List<String> XNAT_GENDERS = Arrays.asList("female", "male", "other", "f", "m");
    public static final String XNAT_DEFAULT_GENDER = "unknown";
    public static final List<String> XNAT_QUALITIES = Arrays.asList("usable", "questionable", "unusable");
    public static final String XNAT_DEFAULT_QUALITY = "";

    //Subject LEVEL known demographics
    public static final Map<String, String> SUBJECT_FIELDS_MAP = new HashMap<String, String>() {{
        //BIDS name, XNAT name
        put("group", "Group");
        put("dx", "Group");
        put("diagnosis", "Group");
    }};
    public static final Map<String, String> SUBJECT_DEMOG_FIELDS_MAP = new HashMap<String, String>() {{
        //BIDS name, XNAT name
        put("src", "Src");
        put("initials", "Initials");
        put("age","Age");
        put("dob","Dob");
        put("yob","Yob");
        put("gender", "Gender");
        put("sex", "Gender");
        put("handedness", "Handedness");
        put("hand", "Handedness");
        put("education", "Education");
        put("educationdesc", "Educationdesc");
        put("race", "Race");
        put("race2", "Race2");
        put("race3", "Race3");
        put("race4", "Race4");
        put("race5", "Race5");
        put("race6", "Race6");
        put("ethnicity", "Ethnicity");
        put("weight", "Weight");
        put("height", "Height");
        put("ses", "Ses");
        put("employment", "Employment");
        put("gestational_age", "GestationalAge");
        put("post_menstrual_age", "PostMenstrualAge");
        put("birth_weight", "BirthWeight");
    }};

    public static final Map<String, BiFunction<BaseElement, String, String>> SUBJECT_DEMOG_TRANSFORM_MAP =
            new HashMap<String, BiFunction<BaseElement, String, String>>() {{
        //If inputs need to be transformed before addition to xnat, add the function to do so, here
        put("Gender", (subject, v) -> {
            v = (v == null) ? "" : v.toLowerCase();
            if (!XNAT_GENDERS.contains(v)) {
                v = XNAT_DEFAULT_GENDER;
            }
            return v;
        });

    }};
    
    public static final Map<String, String> MRSESSION_FIELDS_MAP = new HashMap<String, String>() {{
        //BIDS name (downcased), XNAT name (there must be a method: set + the name, e.g. setDate)
        put("acq_time", "Date");
        put("duration", "Duration");
        put("delay", "Delay");
        put("note", "Note");

        put("age", "Age");

        put("scanner", "Scanner");
        put("scanner_name", "Scanner");
        put("scannername", "Scanner");
        put("scanner_manufacturer", "Scanner_manufacturer");
        put("scannermanufacturer", "Scanner_manufacturer");
        put("manufacturer", "Scanner_manufacturer");
        put("maker", "Scanner_manufacturer");

        put("scanner_model", "Scanner_model");
        put("scannermodel", "Scanner_model");
        put("model", "Scanner_model");

        put("operator", "Operator");

        put("coil", "Coil");
        put("field_strength", "Fieldstrength");
        put("fieldstrength", "Fieldstrength");
        put("marker", "Marker");
        put("stabilization", "Stabilization");
    }};

    public static final Map<String, BiFunction<BaseElement, String, String>> MRSESSION_TRANSFORM_MAP =
            new HashMap<String, BiFunction<BaseElement, String, String>>() {{
        //If inputs need to be transformed before addition to xnat, add the function to do so, here
        put("Date", (session, v) -> {
            if (StringUtils.isBlank(v)) {
                return v;
            }
            // Set the time, too
            // Datetime should be expressed in the following format
            // 2009-06-15T13:45:30 (year, month, day, hour (24h), minute, second; this is equivalent to
            // the RFC3339 “date-time” format, time zone is always assumed as local time)
            SimpleDateFormat formatIn = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            SimpleDateFormat formatOut = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                ((XnatMrsessiondataBean) session).setTime(formatOut.format(formatIn.parse(v)));
            } catch (ParseException e) {
                System.err.println("Issue parsing date from " + v + ": " + e.getMessage());
            }
            return v;
        });

    }};

    public static final ArrayList<String> DataDescriptionKeys = new ArrayList<String>() {{
        add("Name");
        add("BIDSVersion");
        add("License");
        add("Description");
        add("Authors");
        add("Funding");
        add("DatasetDOI");
        add("ReferencesAndLinks");
    }};

    public static final Hashtable<String, String> MRSCAN_FIELD_MAP = new Hashtable<String, String>() {{
        put("acq_time", "Parameters_acqtime");
        // BIDS json sidecar fields (downcased)
        put("seriesdescription", "SeriesDescription");
        put("effectiveechospacing", "Parameters_echospacing");
        put("phaseencodingdirection", "Parameters_phaseencodingdirection");
        //put("sliceencodingdirection", "Parameters_phaseencodingdirection");
        put("scanningsequence", "Parameters_sequence");
        put("sequencevariant", "Parameters_seqvariant");
        put("echotime", "Parameters_te");
        put("inversiontime", "Parameters_ti");
        put("flipangle", "Parameters_flip");
        put("numberofslices", "Frames");
        put("repetitiontime", "Parameters_tr");
        //put("slicetiming", "?");
        put("imagetype","Parameters_imagetype");
        // Misc
        put("notes", "Note");
        put("note", "Note");
        put("quality", "Quality");
        put("QA", "Quality");
    }};

    public static final Map<String, BiFunction<BaseElement, String, String>> MRSCAN_TRANSFORM_MAP =
            new HashMap<String, BiFunction<BaseElement, String, String>>() {{
                //If inputs need to be transformed before addition to xnat, add the function to do so, here
                put("Parameters_imagetype", (scan, v) -> {
                    if (StringUtils.isBlank(v)) {
                        return v;
                    }
                    return v.replaceAll(",","\\\\").replaceAll("[\\[\\]]","");
                });
                put("Quality", (scan, v) -> {
                    v = (v == null) ? "" : v.toLowerCase();
                    if (!XNAT_QUALITIES.contains(v)) {
                        v = XNAT_DEFAULT_QUALITY;
                    }
                    return v;
                });

            }};

    public static final List<String> DATATYPE_FOLDERS = Arrays.asList("anat", "func", "dwi", "fmap");
    public static final ArrayList<String> BIDSJsonKeys = new ArrayList<String>(){{
        add("LongName");
        add("Description");
        add("Levels");
        add("Units");
        add("TermURL");
    }};

    public static final Hashtable<String, String> ANATOMICAL_MODALITIES = new Hashtable<String, String>(){{
        put("T1w", "T1 weighted");
        put("T2w", "T2 weighted");
        put("T1rho", "T1 Rho map");
        put("T1map", "T1 map");
        put("T2map", "T2 map");
        put("T2Star", "T2star");
        put("FLAIR", "FLAIR");
        put("FLASH", "FLASH");
        put("Proton density", "PD");
        put("Proton density map", "PDmap");
        put("Combined PD/T2", "PDT2");
        put("Inplane T1", "inplaneT1");
        put("Inplane T2", "inplaneT2");
        put("Angiography", "angio");
        put("Defacing mask", "defacenask");
        put("Susceptibility Weighted Imaging(SWI)", "SWImagandphase");
    }};

}
