# BIDS Data Importer #

This application traverses a folder which is organized as per [BIDS (Brain Imaging Data Structure) protocol](https://bids.neuroimaging.io/bids_spec.pdf). The folder may be local, or it may be on AWS S3 (in which case, the root folder path ought to be something like `s3://my-bucket/path/to/BIDS`).

# Launching BIDS Data Importer #

Running `java -jar xnat_bids_importer-2.0.jar -h` will print uage info. Note that the presence of `dataset_description.json` is the defining characteristic of a BIDS project root folder per `xnat_bids_importer-2.0.jar`.

# Building jar #

`./gradlew clean shadowJar` Or [download it](../../downloads/xnat_bids_importer-2.0.jar) to `build/libs/xnat_bids_importer-2.0.jar` 

# Building Docker image #

`docker build -t radiologicskate/xnat_bids_importer:2.0 -f container_service/Dockerfile .` Or get it from [dockerhub](https://cloud.docker.com/repository/docker/radiologicskate/xnat_bids_importer)

# Debugging #

Edit the `command.json` from the XNAT interface to set the following: 
```
"ports": {"5006":"5006"},
"environment-variables": {
    "DEBUG": "-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5006"
},
```
Then, connect your debugger to `$XNAT_HOST:5006`. Note that with `suspend=y`, you'll have to connect the debugger before the jar will run. (Note also that you'll need to be able to access port 5006 on your XNAT host machine.)
