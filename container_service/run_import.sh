#!/bin/bash

while getopts "b:a:s:i:" options; do
    case $options in
        b ) echo "AWS S3 BIDS directory: $OPTARG"
            bids="$OPTARG";;
        a ) echo "AWS S3 access key: $OPTARG"
            key=$OPTARG;;
        s ) echo "AWS S3 secret key: *****"
            secret=$OPTARG;;
        i ) echo "XNAT project: $OPTARG"
            project=$OPTARG;;
        \?) echo "Unknown option"
            exit 1;;
    esac
done

if [[ $XNAT_HOST =~ localhost ]]; then
    XNAT_HOST=${XNAT_HOST/localhost/host.docker.internal}
fi

/usr/bin/java $DEBUG -jar /usr/local/bin/xnat_bids_importer.jar \
    -t $XNAT_HOST -u $XNAT_USER -p $XNAT_PASS -l /output/logs \
    -b $bids -a $key -s $secret -i $project

result=$?
echo "Exiting with code $result"
exit $result
